FROM openjdk:14-alpine as builder

WORKDIR /api

COPY gradle gradle
COPY gradlew .

RUN sh gradlew --version

COPY build.gradle .
COPY settings.gradle .
COPY src src

RUN sh gradlew startShadowScripts

FROM openjdk:14-alpine

COPY --from=builder /api/build/libs/presenter-api-all.jar server.jar
ENTRYPOINT java $JAVA_OPTS -jar server.jar
