#!/usr/bin/env bash

cd "$(dirname "$0")" || exit

docker build -t presenter-api .
kubectl delete -n presenter "$(kubectl get pod --selector app=presenter-api -o name -n presenter)"
