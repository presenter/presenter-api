package org.schemea.presenter.api;

import com.sun.net.httpserver.HttpServer;
import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.changelog.ChangeLogParameters;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.parser.ChangeLogParser;
import liquibase.parser.ChangeLogParserFactory;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.http.RequestHandler;
import org.schemea.presenter.api.config.DatabaseConnection;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class Application {

    private static DatabaseChangeLog getLiquibaseChangelog(ResourceAccessor resourceAccessor) throws LiquibaseException {
        final String changelogPath = "db/changelog.xml";
        final ChangeLogParser parser = ChangeLogParserFactory.getInstance().getParser(changelogPath, resourceAccessor);

        return parser.parse(changelogPath, new ChangeLogParameters(), resourceAccessor);
    }

    private static void runLiquibase() throws LiquibaseException, SQLException {
        final ClassLoaderResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor();
        final DatabaseConnection db = Database.getConnectionInfo();
        final DatabaseChangeLog changelog = getLiquibaseChangelog(resourceAccessor);

        final Connection connection = DriverManager.getConnection(
                db.getUrl(),
                db.getUsername(), db.getPassword()
        );

        final liquibase.database.Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
        database.setLiquibaseSchemaName("public");

        Liquibase liquibase = new Liquibase(changelog, resourceAccessor, database);
        liquibase.dropAll();
        liquibase.update(new Contexts());
    }

    public static void main(String[] args) {
        try {
            runLiquibase();

            Database.initialize();

            int port = 8081;

            try {
                String env = System.getenv("PORT");

                if (Objects.nonNull(env) && !env.isEmpty()) {
                    port = Integer.parseInt(env);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
            server.createContext("/presenter-api", new RequestHandler());
            server.start();
            final InetSocketAddress address = server.getAddress();
            System.out.printf("Listening on %s:%s\n", address.getHostName(), address.getPort());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
