package org.schemea.presenter.api.config;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.schemea.presenter.api.mappers.*;
import org.schemea.presenter.api.typehandler.UUIDTypeHandler;

import javax.sql.DataSource;
import java.text.MessageFormat;
import java.util.Objects;
import java.util.UUID;

public class Database {

    private static SqlSessionManager sessionManager;

    public static void initialize() {
        sessionManager = SqlSessionManager.newInstance(buildSessionFactory());
    }

    private static String getDatabaseURL() {
        String host = System.getenv("POSTGRES_HOST");

        if (Objects.isNull(host) || host.isEmpty()) {
            host = "presenter-api-postgres-service:5432";
        }

        return MessageFormat.format("jdbc:postgresql://{0}/presenter-api", host);
    }

    public static DatabaseConnection getConnectionInfo() {
        final String username = System.getenv("POSTGRES_USER");
        final String password = System.getenv("POSTGRES_PASSWORD");

        if (Objects.isNull(username) || Objects.isNull(password)) {
            throw new IllegalStateException("Missing postgres credentials");
        }

        return DatabaseConnection.builder()
                .url(getDatabaseURL())
                .username(username)
                .password(password)
                .build();
    }

    public static SqlSessionFactory buildSessionFactory() {
        final DatabaseConnection connection = getConnectionInfo();

        DataSource dataSource = new PooledDataSource(
                "org.postgresql.Driver",
                connection.getUrl(),
                connection.getUsername(), connection.getPassword()
        );

        Environment environment = new Environment("ef7c26a3-f04a-43ee-a569-8236d1b786c8", new JdbcTransactionFactory(), dataSource);
        Configuration configuration = new Configuration(environment);
        configuration.addMapper(BoardMapper.class);
        configuration.addMapper(PersonMapper.class);
        configuration.addMapper(ProjectMapper.class);
        configuration.addMapper(StatusMapper.class);
        configuration.addMapper(StoryMapper.class);
        configuration.addMapper(SnapshotMapper.class);
        configuration.addMapper(RoleMapper.class);
        // configuration.setMapUnderscoreToCamelCase();

        final TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
        typeHandlerRegistry.register(UUIDTypeHandler.class);

        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

        return builder.build(configuration);
    }

    public static SqlSessionManager getSessionManager() {
        return sessionManager;
    }
}
