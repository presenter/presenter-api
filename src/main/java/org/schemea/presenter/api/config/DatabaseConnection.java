package org.schemea.presenter.api.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class DatabaseConnection {
    private final String url;
    private final String username;
    private final String password;
}
