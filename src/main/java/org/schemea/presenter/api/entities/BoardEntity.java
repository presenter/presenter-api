package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.schema.DataFetchingEnvironment;
import lombok.*;
import org.apache.ibatis.session.SqlSessionManager;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.mappers.StatusMapper;
import org.schemea.presenter.api.mappers.StoryMapper;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BoardEntity {
    @GraphQLField
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private String name;

    @GraphQLField
    @GraphQLNonNull
    private UUID snapshot_id;

    @GraphQLField
    @GraphQLNonNull
    public Set<StoryEntity> stories(DataFetchingEnvironment env) {
        final SqlSessionManager sessionManager = Database.getSessionManager();
        final StoryMapper mapper = sessionManager.getMapper(StoryMapper.class);
        final BoardEntity source = env.<BoardEntity>getSource();

        return mapper.findByBoard(source.snapshot_id, source.id);
    }

    @GraphQLField
    @GraphQLNonNull
    public List<StatusEntity> statuses(DataFetchingEnvironment env) {
        final SqlSessionManager sessionManager = Database.getSessionManager();
        final StatusMapper mapper = sessionManager.getMapper(StatusMapper.class);
        final BoardEntity source = env.<BoardEntity>getSource();

        return mapper.findByBoard(source.snapshot_id, source.id);
    }
}
