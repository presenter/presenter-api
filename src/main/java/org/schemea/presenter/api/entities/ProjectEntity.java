package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.schema.DataFetchingEnvironment;
import lombok.*;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.mappers.SnapshotMapper;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEntity {
    @GraphQLID
    @GraphQLField
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private String name;

    @GraphQLField
    @GraphQLNonNull
    private UUID default_snapshot;

    @GraphQLField
    @GraphQLNonNull
    public Set<SnapshotEntity> snapshots(DataFetchingEnvironment env) {
        final UUID project = env.<ProjectEntity>getSource().getId();
        final SnapshotMapper mapper = Database.getSessionManager().getMapper(SnapshotMapper.class);

        return mapper.findByProjectID(project);
    }
}
