package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity {
    @GraphQLID
    @GraphQLField
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private UUID user_id;

    @GraphQLField
    @GraphQLNonNull
    private UUID project_id;

    @GraphQLField
    @GraphQLNonNull
    private String name;

    @GraphQLField
    @GraphQLNonNull
    private Boolean locked;
}
