package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.schema.DataFetchingEnvironment;
import lombok.*;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.mappers.BoardMapper;
import org.schemea.presenter.api.mappers.StoryMapper;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SnapshotEntity {
    @GraphQLField
    @GraphQLID
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private UUID project_id;

    @GraphQLField
    @GraphQLNonNull
    private OffsetDateTime timestamp;

    @GraphQLField
    @GraphQLNonNull
    private String name;

    @GraphQLField
    private String description;

    @GraphQLField
    @GraphQLNonNull
    public Set<StoryEntity> stories(DataFetchingEnvironment env) {
        final SnapshotEntity source = env.<SnapshotEntity>getSource();
        final StoryMapper mapper = Database.getSessionManager().getMapper(StoryMapper.class);

        return mapper.findBySnapshotID(source.id);
    }

    @GraphQLField
    @GraphQLNonNull
    public Set<BoardEntity> boards(DataFetchingEnvironment env) {
        final SnapshotEntity source = env.<SnapshotEntity>getSource();
        final BoardMapper mapper = Database.getSessionManager().getMapper(BoardMapper.class);

        return mapper.findBySnapshotID(source.id);
    }
}
