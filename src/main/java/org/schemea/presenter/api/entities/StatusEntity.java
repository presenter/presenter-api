package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLName;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.schema.DataFetchingEnvironment;
import lombok.*;
import org.schemea.presenter.api.sql.Persistence;
import org.schemea.presenter.api.sql.SqlOperation;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.mappers.StatusMapper;
import org.schemea.presenter.api.mappers.StoryMapper;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StatusEntity {
    @GraphQLID
    @GraphQLField
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private UUID snapshot_id;

    @GraphQLField
    @GraphQLNonNull
    private String name;
    @GraphQLField
    @GraphQLNonNull
    private UUID board_id;

    @Builder.Default
    @Persistence(SqlOperation.SELECT)
    private List<UUID> stories = new ArrayList<>();

    private List<StoryEntity> buildStoryOrder(StatusEntity source) {
        final StoryMapper mapper = Database.getSessionManager().getMapper(StoryMapper.class);

        final Set<StoryEntity> stories = mapper.findByStatus(source.snapshot_id, source.id);

        final Map<UUID, StoryEntity> map = stories.stream()
                .collect(Collectors.toMap(StoryEntity::getId, Function.identity()));

        return Stream.concat(
                source.stories.stream()
                        .map(map::get)
                        .filter(Objects::nonNull),
                stories.stream()
                        .filter(value -> !source.stories.contains(value.getId()))
        ).collect(Collectors.toList());
    }

    @GraphQLField
    @GraphQLName("stories")
    @GraphQLNonNull
    public List<StoryEntity> storyOrder(DataFetchingEnvironment env) {
        final StatusEntity source = env.getSource();
        final StatusMapper mapper = Database.getSessionManager().getMapper(StatusMapper.class);

        final List<StoryEntity> stories = buildStoryOrder(source);

        if (stories.size() > source.stories.size()) {
            mapper.updateStoryOrder(
                    source.snapshot_id,
                    source.id,
                    stories.stream()
                            .map(StoryEntity::getId)
                            .collect(Collectors.toList())
            );
        }

        return stories;
    }
}
