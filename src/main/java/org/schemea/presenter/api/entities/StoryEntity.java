package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.schema.DataFetchingEnvironment;
import lombok.*;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.mappers.BoardMapper;
import org.schemea.presenter.api.mappers.ProjectMapper;
import org.schemea.presenter.api.mappers.StatusMapper;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoryEntity {
    @GraphQLID
    @GraphQLField
    @GraphQLNonNull
    private UUID id;
    @GraphQLField
    @GraphQLNonNull
    private String title;
    @GraphQLField
    private String description;
    @GraphQLField
    private UUID status_id;
    @GraphQLField
    @GraphQLNonNull
    private UUID snapshot_id;

    @GraphQLField
    private Integer value;
    @GraphQLField
    private Integer effort;

    public StoryEntity status(DataFetchingEnvironment env) {
        final StoryEntity story = env.<StoryEntity>getSource();
        final StatusMapper mapper = Database.getSessionManager().getMapper(StatusMapper.class);

        return mapper.findById(story.snapshot_id, story.status_id);
    }
    public BoardEntity board(DataFetchingEnvironment env) {
        final StoryEntity story = env.<StoryEntity>getSource();
        final BoardMapper mapper = Database.getSessionManager().getMapper(BoardMapper.class);

        return mapper.findByStoryID(story.getId());
    }

    public ProjectEntity project(DataFetchingEnvironment env) {
        final StoryEntity story = env.<StoryEntity>getSource();
        final ProjectMapper mapper = Database.getSessionManager().getMapper(ProjectMapper.class);

        return mapper.findByStoryID(story.getId());
    }
}
