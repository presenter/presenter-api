package org.schemea.presenter.api.entities;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    @GraphQLID
    @GraphQLField
    @GraphQLNonNull
    private UUID id;

    @GraphQLField
    @GraphQLNonNull
    private String username;
}
