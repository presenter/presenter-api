package org.schemea.presenter.api.exceptions;

import lombok.Getter;

@Getter
public class RequestException extends RuntimeException {
    final private int status;

    public RequestException(int status, String message) {
        super(message);
        this.status = status;
    }
}
