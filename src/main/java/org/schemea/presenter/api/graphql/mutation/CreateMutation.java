package org.schemea.presenter.api.graphql.mutation;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLInvokeDetached;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;
import org.schemea.presenter.api.entities.*;
import org.schemea.presenter.api.graphql.Mutation;
import org.schemea.presenter.api.mappers.*;

import java.util.UUID;
import java.util.stream.Stream;

@GraphQLTypeExtension(Mutation.class)
public class CreateMutation extends Mutation {
    @GraphQLField
    @GraphQLInvokeDetached
    public UUID newSnapshot(@GraphQLNonNull UUID project_id, @GraphQLNonNull String name) {
        final SnapshotMapper mapper = sessionManager.getMapper(SnapshotMapper.class);

        final SnapshotEntity snapshot = SnapshotEntity.builder()
                .project_id(project_id)
                .name(name)
                .build();

        return mapper.insert(snapshot);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public UUID newBoard(@GraphQLNonNull UUID snapshot_id, @GraphQLNonNull String name) {
        final BoardMapper mapper = sessionManager.getMapper(BoardMapper.class);

        final BoardEntity board = BoardEntity.builder()
                .name(name)
                .snapshot_id(snapshot_id)
                .build();

        return mapper.insert(board);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public UUID newStory(@GraphQLNonNull UUID snapshot_id, @GraphQLNonNull String title, String description, UUID status_id) {
        final StoryMapper mapper = sessionManager.getMapper(StoryMapper.class);

        final StoryEntity story = StoryEntity.builder()
                .title(title)
                .description(description)
                .status_id(status_id)
                .snapshot_id(snapshot_id)
                .build();

        return mapper.insert(story);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public UUID newStatus(@GraphQLNonNull UUID snapshot_id, @GraphQLNonNull UUID board_id, @GraphQLNonNull String name) {
        final StatusMapper mapper = sessionManager.getMapper(StatusMapper.class);

        final StatusEntity status = StatusEntity.builder()
                .snapshot_id(snapshot_id)
                .name(name)
                .board_id(board_id)
                .build();

        return mapper.insert(status);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public UUID newProject(@GraphQLNonNull String name) {
        final ProjectMapper mapper = sessionManager.getMapper(ProjectMapper.class);
        final SnapshotMapper snapshotMapper = sessionManager.getMapper(SnapshotMapper.class);
        final RoleMapper roleMapper = sessionManager.getMapper(RoleMapper.class);

        final UUID defaultSnapshot = UUID.randomUUID();

        final UUID project = mapper.insert(
                ProjectEntity.builder()
                        .name(name)
                        .default_snapshot(defaultSnapshot)
                        .build()
        );

        snapshotMapper.insert(
                SnapshotEntity.builder()
                        .id(defaultSnapshot)
                        .project_id(project)
                        .name("default")
                        .build()
        );

        Stream.of("owner", "editor", "viewer")
                .map(role ->
                        RoleEntity.builder()
                                .locked(true)
                                .name(role)
                                .project_id(project)
                                .build()
                )
                .forEach(roleMapper::insert);

        return project;
    }
}
