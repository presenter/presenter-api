package org.schemea.presenter.api.graphql.mutation;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLInvokeDetached;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;
import org.schemea.presenter.api.entities.*;
import org.schemea.presenter.api.graphql.Mutation;
import org.schemea.presenter.api.mappers.*;

import java.util.List;
import java.util.UUID;

@GraphQLTypeExtension(Mutation.class)
public class UpdateMutation extends Mutation {
    @GraphQLField
    @GraphQLNonNull
    @GraphQLInvokeDetached
    public UUID cloneSnapshot(@GraphQLNonNull UUID source, @GraphQLNonNull String name) {
        try {
            sessionManager.startManagedSession();

            final SnapshotMapper mapper = sessionManager.getMapper(SnapshotMapper.class);

            final SnapshotEntity newSnapshot = mapper.findByID(source);
            newSnapshot.setId(UUID.randomUUID());
            newSnapshot.setName(name);
            newSnapshot.setTimestamp(null);

            mapper.insert(newSnapshot);
            mapper.clone(source, newSnapshot.getId());

            sessionManager.commit();

            return newSnapshot.getId();
        } finally {
            sessionManager.close();
        }
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public String updateStoryStatus(@GraphQLNonNull UUID snapshot, @GraphQLNonNull UUID story, @GraphQLNonNull UUID status) {
        final StatusMapper mapper = sessionManager.getMapper(StatusMapper.class);
        mapper.updateStatus(snapshot, story, status);
        sessionManager.commit();
        return null;
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public String updateStoryOrder(@GraphQLNonNull UUID snapshot, @GraphQLNonNull UUID status, @GraphQLNonNull List<UUID> order) {
        final StatusMapper mapper = sessionManager.getMapper(StatusMapper.class);
        mapper.updateStoryOrder(snapshot, status, order);
        return null;
    }
}
