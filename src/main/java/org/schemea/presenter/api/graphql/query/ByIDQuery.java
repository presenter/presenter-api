package org.schemea.presenter.api.graphql.query;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLInvokeDetached;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;
import graphql.schema.DataFetchingEnvironment;
import org.schemea.presenter.api.entities.BoardEntity;
import org.schemea.presenter.api.entities.ProjectEntity;
import org.schemea.presenter.api.entities.SnapshotEntity;
import org.schemea.presenter.api.entities.StoryEntity;
import org.schemea.presenter.api.exceptions.RequestException;
import org.schemea.presenter.api.graphql.Query;
import org.schemea.presenter.api.http.Auth;
import org.schemea.presenter.api.mappers.BoardMapper;
import org.schemea.presenter.api.mappers.ProjectMapper;
import org.schemea.presenter.api.mappers.SnapshotMapper;
import org.schemea.presenter.api.mappers.StoryMapper;

import java.util.Objects;
import java.util.UUID;

@GraphQLTypeExtension(Query.class)
public class ByIDQuery extends Query {

    @GraphQLField
    @GraphQLInvokeDetached
    public BoardEntity board(DataFetchingEnvironment env, @GraphQLNonNull UUID snapshot, @GraphQLNonNull UUID id) {
        final BoardMapper mapper = sessionManager.getMapper(BoardMapper.class);

        Auth.requireBoardRole(env, id, "viewer");

        return mapper.findByID(snapshot, id);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public StoryEntity story(DataFetchingEnvironment env, @GraphQLNonNull UUID snapshot, @GraphQLNonNull UUID id) {
        final StoryMapper mapper = sessionManager.getMapper(StoryMapper.class);
        final StoryEntity story = mapper.findByID(snapshot, id);
        final BoardEntity board = story.board(env);

        if (Objects.nonNull(board)) {
            Auth.requireBoardRole(env, board.getId(), "viewer");
        } else {
            Auth.requireProjectRole(env, story.getId(), "viewer");
        }

        return story;
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public ProjectEntity project(DataFetchingEnvironment env, @GraphQLNonNull UUID id) {
        final ProjectMapper mapper = sessionManager.getMapper(ProjectMapper.class);
        Auth.requireProjectRole(env, id, "viewer");
        return mapper.findByID(id);
    }

    @GraphQLField
    @GraphQLInvokeDetached
    public SnapshotEntity snapshot(DataFetchingEnvironment env, @GraphQLNonNull UUID id) {
        final SnapshotMapper mapper = sessionManager.getMapper(SnapshotMapper.class);
        final SnapshotEntity snapshot = mapper.findByID(id);
        Auth.requireProjectRole(env, snapshot.getProject_id(), "viewer");
        return snapshot;
    }
}
