package org.schemea.presenter.api.graphql.query;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLInvokeDetached;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;
import graphql.schema.DataFetchingEnvironment;
import org.schemea.presenter.api.entities.*;
import org.schemea.presenter.api.exceptions.RequestException;
import org.schemea.presenter.api.graphql.Query;
import org.schemea.presenter.api.http.Auth;
import org.schemea.presenter.api.mappers.*;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@GraphQLTypeExtension(Query.class)
public class ProjectQuery extends Query {
    @GraphQLField
    @GraphQLNonNull
    @GraphQLInvokeDetached
    public Set<ProjectEntity> allProjects() {
        final ProjectMapper mapper = sessionManager.getMapper(ProjectMapper.class);
        return mapper.findAll();
    }

    @GraphQLField
    @GraphQLNonNull
    @GraphQLInvokeDetached
    public ProjectEntity projectByBoardID(DataFetchingEnvironment env, @GraphQLNonNull UUID board) {
        final ProjectMapper mapper = sessionManager.getMapper(ProjectMapper.class);

        final ProjectEntity project = mapper.findByBoardID(board);

        Auth.requireProjectRole(env, project.getId(), "viewer");

        return project;
    }
}
