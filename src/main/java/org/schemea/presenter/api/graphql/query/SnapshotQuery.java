package org.schemea.presenter.api.graphql.query;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLInvokeDetached;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import graphql.annotations.annotationTypes.GraphQLTypeExtension;
import org.schemea.presenter.api.entities.SnapshotEntity;
import org.schemea.presenter.api.graphql.Query;
import org.schemea.presenter.api.mappers.SnapshotMapper;

import java.util.Set;
import java.util.UUID;

@GraphQLTypeExtension(Query.class)
public class SnapshotQuery extends Query {

    @GraphQLField
    @GraphQLNonNull
    @GraphQLInvokeDetached
    public Set<SnapshotEntity> snapshotsByProjectID(@GraphQLNonNull UUID project_id) {
        final SnapshotMapper mapper = sessionManager.getMapper(SnapshotMapper.class);
        return mapper.findByProjectID(project_id);
    }

    @GraphQLField
    @GraphQLNonNull
    @GraphQLInvokeDetached
    public Set<SnapshotEntity> snapshotsByBoardID(@GraphQLNonNull UUID board_id) {
        final SnapshotMapper mapper = sessionManager.getMapper(SnapshotMapper.class);
        return mapper.findByBoardID(board_id);
    }
}
