package org.schemea.presenter.api.http;

import graphql.schema.DataFetchingEnvironment;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.entities.ProjectEntity;
import org.schemea.presenter.api.entities.RoleEntity;
import org.schemea.presenter.api.exceptions.RequestException;
import org.schemea.presenter.api.mappers.BoardMapper;
import org.schemea.presenter.api.mappers.ProjectMapper;
import org.schemea.presenter.api.mappers.RoleMapper;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Auth {
    public static List<RoleEntity> getProjectRoles(DataFetchingEnvironment env, UUID project) {
        final RequestContext context = env.<RequestContext>getContext();
        final Jws<Claims> jwt = context.getJwt();

        if (Objects.isNull(jwt)) {
            return List.of();
        }

        final String subject = jwt.getBody().getSubject();

        if (Objects.isNull(subject) || subject.isEmpty()) {
            return List.of();
        }

        final RoleMapper mapper = Database.getSessionManager().getMapper(RoleMapper.class);
        return mapper.findByProjectID(UUID.fromString(subject), project);
    }

    public static List<RoleEntity> getBoardRoles(DataFetchingEnvironment env, UUID board) {
        final ProjectMapper mapper = Database.getSessionManager().getMapper(ProjectMapper.class);
        final ProjectEntity project = mapper.findByBoardID(board);

        return getProjectRoles(env, project.getId());
    }

    public static boolean hasProjectRole(DataFetchingEnvironment env, UUID project, String role) {
        return getProjectRoles(env, project).stream()
                .anyMatch(r -> r.getName().equals(role));
    }

    public static boolean hasBoardRole(DataFetchingEnvironment env, UUID board, String role) {
        return getBoardRoles(env, board).stream()
                .anyMatch(r -> r.getName().equals(role));
    }

    public static void requireProjectRole(DataFetchingEnvironment env, UUID project, String role) {
        if (!hasBoardRole(env, project, role)) {
            throw new RequestException(404, "user does not have sufficient permission");
        }
    }

    public static void requireBoardRole(DataFetchingEnvironment env, UUID board, String role) {
        if (!hasBoardRole(env, board, role)) {
            throw new RequestException(404, "user does not have sufficient permission");
        }
    }
}
