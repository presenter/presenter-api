package org.schemea.presenter.api.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.jr.ob.JSON;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import graphql.*;
import graphql.annotations.AnnotationsSchemaCreator;
import graphql.annotations.processor.GraphQLAnnotations;
import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLSchema;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.security.SignatureException;
import lombok.Getter;
import org.apache.ibatis.session.SqlSessionManager;
import org.schemea.presenter.api.config.Database;
import org.schemea.presenter.api.config.ExtendedFunction;
import org.schemea.presenter.api.graphql.Mutation;
import org.schemea.presenter.api.graphql.Query;
import org.schemea.presenter.api.graphql.mutation.CreateMutation;
import org.schemea.presenter.api.graphql.mutation.UpdateMutation;
import org.schemea.presenter.api.graphql.query.ByIDQuery;
import org.schemea.presenter.api.graphql.query.ProjectQuery;
import org.schemea.presenter.api.graphql.query.SnapshotQuery;
import org.schemea.presenter.api.services.TokenService;

import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Getter
public class RequestHandler implements HttpHandler {
    private final GraphQLSchema schema;
    private final GraphQL graphQL;
    private final TokenService jwtParser = new TokenService();

    public RequestHandler() throws InvalidKeySpecException, CertificateException, NoSuchAlgorithmException, IOException {
        schema = createSchema();
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private GraphQLSchema createSchema() {
        final GraphQLAnnotations graphQLAnnotations = new GraphQLAnnotations();

        graphQLAnnotations.registerTypeExtension(CreateMutation.class);
        graphQLAnnotations.registerTypeExtension(UpdateMutation.class);

        graphQLAnnotations.registerTypeExtension(ByIDQuery.class);
        graphQLAnnotations.registerTypeExtension(ProjectQuery.class);
        graphQLAnnotations.registerTypeExtension(SnapshotQuery.class);

        return AnnotationsSchemaCreator.newAnnotationsSchema()
                .setAnnotationsProcessor(graphQLAnnotations)
                .query(Query.class)
                .mutation(Mutation.class)
                .typeFunction(new ExtendedFunction(ExtendedScalars.UUID, UUID.class))
                .typeFunction(new ExtendedFunction(ExtendedScalars.DateTime, OffsetDateTime.class))
                .typeFunction(new ExtendedFunction(ExtendedScalars.Date, LocalDate.class))
                .build();
    }

    private String extractQuery(String data) throws IOException {
        final Map<String, Object> map = JSON.std.mapFrom(data);
        return map.get("query").toString();
    }

    private Map<String, String> parseCookies(List<String> cookies) {
        if (Objects.isNull(cookies)) {
            return Map.of();
        } else {
            return cookies.stream()
                    .map(value -> value.split(";"))
                    .flatMap(Arrays::stream)
                    .map(pair -> pair.split("="))
                    .collect(Collectors.toMap(pair -> pair[0], pair -> pair[1]));
        }
    }

    private void rejectRequest(HttpExchange exchange, int code, String message) {
        try (final OutputStream body = exchange.getResponseBody()) {
            exchange.sendResponseHeaders(code, 0);

            try (OutputStream stream = exchange.getResponseBody()) {
                stream.write(message.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        final Headers headers = exchange.getRequestHeaders();

        final Map<String, String> cookies = parseCookies(headers.get("cookie"));

        Jws<Claims> jwt = null;

        if (cookies.containsKey("JWT-SIGNATURE")) {
            final String csrf = cookies.get("CSRF");
            final String rawJwt = headers.getFirst("JWT") + "." + headers.getFirst("JWT-SIGNATURE");

            if (Objects.isNull(csrf)) {
                rejectRequest(exchange, 401, "missing csrf token");
            }

            if (!csrf.equals(headers.getFirst("X-CSRF-TOKEN"))) {
                rejectRequest(exchange, 401, "invalid csrf token");
            }

            try {
                jwt = jwtParser.parse(rawJwt);
            } catch (SignatureException e) {
                rejectRequest(exchange, 401, "invalid jwt");
            } catch (ExpiredJwtException e) {
                rejectRequest(exchange, 401, "expired jwt");
            } catch (Exception e) {
                rejectRequest(exchange, 500, "an error occurred during jwt parsing");
            }
        }

        final SqlSessionManager sessionManager = Database.getSessionManager();

        try {
            sessionManager.startManagedSession();

            final RequestContext context = RequestContext
                    .builder()
                    .jwt(jwt)
                    .exchange(exchange)
                    .build();

            final String query = extractQuery(new String(exchange.getRequestBody().readAllBytes()));

            final ExecutionInput input = ExecutionInput
                    .newExecutionInput(query)
                    .context(context)
                    .build();

            final ExecutionResult result = graphQL.execute(input);
            final List<GraphQLError> errors = result.getErrors();
            final Object data = result.getData();

            exchange.sendResponseHeaders(200, 0);

            try (final OutputStream stream = exchange.getResponseBody()) {
                final ObjectMapper mapper = new ObjectMapper();
                final ObjectNode root = mapper.createObjectNode();

                root.putPOJO("data", data);

                if (!errors.isEmpty()) {
                    final ArrayNode node = root.putArray("errors");

                    errors.stream()
                            .map(GraphQLError::toSpecification)
                            .map(Map::values)
                            .flatMap(Collection::stream)
                            .map(Object::toString)
                            .forEach(node::add);
                }

                mapper.writeValue(stream, root);
            }

            sessionManager.commit();
        } catch (RuntimeException e) {
            e.printStackTrace();
            sessionManager.rollback();

            exchange.sendResponseHeaders(500, 0);
        } finally {
            sessionManager.close();
        }
    }
}
