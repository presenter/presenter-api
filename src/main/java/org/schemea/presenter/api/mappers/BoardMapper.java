package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.schemea.presenter.api.entities.BoardEntity;
import org.schemea.presenter.api.sql.Sql;

import java.util.Set;
import java.util.UUID;

@Mapper
public interface BoardMapper {

    @Select("SELECT * FROM boards WHERE snapshot_id = #{snapshot} AND id = #{id}")
    BoardEntity findByID(UUID snapshot, UUID id);

    @Select("SELECT * FROM boards WHERE snapshot_id = #{snapshot}")
    Set<BoardEntity> findBySnapshotID(UUID snapshot);

    @Select("SELECT b.* FROM stories sto " +
            "JOIN statuses sta ON sto.status_id = sta.id AND sto.snapshot_id = sta.snapshot_id " +
            "JOIN boards b ON b.id = sta.board_id AND b.snapshot_id = sta.snapshot_id")
    BoardEntity findByStoryID(UUID story);

    // @Select("INSERT INTO boards(name, project_id) VALUES (#{name}, #{}) RETURNING id")
    default UUID insert(BoardEntity value) {
        return Sql.insert("boards", value);
    }
}
