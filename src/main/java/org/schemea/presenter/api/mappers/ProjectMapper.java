package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.schemea.presenter.api.entities.ProjectEntity;
import org.schemea.presenter.api.sql.Sql;

import java.util.Set;
import java.util.UUID;

public interface ProjectMapper extends SqlMapper<ProjectEntity> {

    @Select("SELECT * FROM projects")
    Set<ProjectEntity> findAll();

    @Select("SELECT * FROM projects WHERE id = #{id}")
    ProjectEntity findByID(UUID id);

    @Select("SELECT p.* FROM boards b JOIN snapshots s ON s.id = b.snapshot_id JOIN projects p ON p.id = s.project_id WHERE b.id = #{board} LIMIT 1")
    ProjectEntity findByBoardID(UUID board);

    @Select("SELECT p.* FROM stories s JOIN snapshots snap ON s.snapshot_id = snap.id JOIN projects p ON p.id = snap.project_id")
    ProjectEntity findByStoryID(UUID story);

    default UUID insert(ProjectEntity value) { return Sql.insert("projects", value);}

    @Update("UPDATE projects SET default_snapshot = #{snapshot} WHERE id = #{project}")
    void updateDefaultSnapshot(UUID project, UUID snapshot);
}
