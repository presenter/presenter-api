package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.schemea.presenter.api.entities.RoleEntity;
import org.schemea.presenter.api.sql.Sql;

import java.util.List;
import java.util.UUID;

public interface RoleMapper extends SqlMapper {
    @Select("SELECT * FROM access_roles WHERE id = #{id}")
    RoleEntity findByID(UUID id);

    @Select("SELECT * FROM user_access_role WHERE user_id = #{user} AND project_id = #{project}")
    List<RoleEntity> findByProjectID(UUID user, UUID project);

    default UUID insert(RoleEntity role) { return Sql.insert("access_roles", role); }
}
