package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.schemea.presenter.api.entities.SnapshotEntity;
import org.schemea.presenter.api.sql.Sql;

import java.util.Set;
import java.util.UUID;

public interface SnapshotMapper extends SqlMapper<SnapshotEntity> {

    @Select("SELECT * FROM snapshots WHERE id = #{id}")
    SnapshotEntity findByID(UUID id);

    @Select("SELECT * FROM snapshots WHERE name = #{name}")
    SnapshotEntity findByName(String name);

    @Select("SELECT * FROM snapshots WHERE project_id = #{project}")
    Set<SnapshotEntity> findByProjectID(UUID project);

    @Select("SELECT s2.* FROM boards b JOIN snapshots s ON s.id = b.snapshot_id JOIN snapshots s2 ON s2.project_id = s.project_id WHERE b.id = #{id}")
    Set<SnapshotEntity> findByBoardID(UUID id);

    @Update("SELECT clone_snapshot(#{source}, #{target})")
    void clone(UUID source, UUID target);

    default UUID insert(SnapshotEntity value) {
        return Sql.insert("snapshots", value);
    }
}
