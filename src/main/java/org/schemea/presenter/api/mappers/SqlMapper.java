package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;

import java.util.List;

@Mapper
public interface SqlMapper<T> {
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    T selectOne(SelectStatementProvider statement);

    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<T> selectMany(SelectStatementProvider statement);

    @SelectProvider(type = SqlProviderAdapter.class, method = "insert")
    int insertOne(InsertStatementProvider<T> statement);

    @InsertProvider(type = SqlProviderAdapter.class, method = "insert")
    List<T> insertMany(InsertStatementProvider<T> statement);
}
