package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.schemea.presenter.api.entities.StatusEntity;
import org.schemea.presenter.api.entities.StoryEntity;
import org.schemea.presenter.api.sql.Sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface StatusMapper extends SqlMapper<StatusEntity> {
    default List<StatusEntity> findByBoard(UUID snapshot, UUID board) {
        return Sql.select("statuses", StatusEntity.class,
                StatusEntity.builder()
                        .snapshot_id(snapshot)
                        .board_id(board)
                        .build()
        );
    }

    default UUID insert(StatusEntity value) {
        return Sql.insert("statuses", value);
    }


    @Select("SELECT * FROM statuses WHERE snapshot_id = #{snapshot} AND id = #{id}")
    StoryEntity findById(UUID snapshot, UUID id);

    @Update("UPDATE stories SET status_id = #{status} WHERE snapshot_id = #{snapshot} AND id= #{story}")
    void updateStatus(UUID snapshot, UUID story, UUID status);

    default void updateStoryOrder(UUID snapshot, UUID id, List<UUID> order) {
        final Connection connection = Sql.getConnection();

        try (PreparedStatement statement = connection.prepareStatement("UPDATE statuses SET stories = ? WHERE snapshot_id = ? AND id = ?")) {
            statement.setArray(1, connection.createArrayOf("uuid", order.toArray()));
            statement.setObject(2, snapshot);
            statement.setObject(3, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
