package org.schemea.presenter.api.mappers;

import org.apache.ibatis.annotations.Select;
import org.schemea.presenter.api.entities.StoryEntity;
import org.schemea.presenter.api.sql.Sql;

import java.util.Set;
import java.util.UUID;

public interface StoryMapper extends SqlMapper<StoryEntity> {
    @Select("SELECT * FROM stories WHERE snapshot_id = #{snapshot} AND id = #{id}")
    StoryEntity findByID(UUID snapshot, UUID id);

    @Select("SELECT s.* FROM statuses st JOIN stories s ON s.status_id = st.id  WHERE s.snapshot_id = #{snapshot} st.board_id = #{board}")
    Set<StoryEntity> findByBoard(UUID snapshot, UUID board);

    @Select("SELECT * FROM stories WHERE snapshot_id = #{snapshot}")
    Set<StoryEntity> findBySnapshotID(UUID snapshot);

    @Select("SELECT * FROM stories WHERE status_id = #{status} AND snapshot_id = #{snapshot}")
    Set<StoryEntity> findByStatus(UUID snapshot, UUID status);

    // @Select({ "INSERT " +
    //         "INTO stories(title, description, status_id) " +
    //         "VALUES (#{title}, #{description}, #{getStatus_id().toString()}) RETURNING id" })
    default UUID insert(StoryEntity story) {
        return Sql.insert("stories", story);
    }
}
