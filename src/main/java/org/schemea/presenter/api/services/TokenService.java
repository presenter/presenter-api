package org.schemea.presenter.api.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class TokenService {
    private PrivateKey privateKey;
    private PublicKey publicKey;

    public static String USERNAME = "username";
    public static String AUTHORITIES = "authorities";

    public TokenService() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, CertificateException {
        loadPrivateKey(System.getenv("PRIVATE_KEY"));
        loadPublicKey(System.getenv("PUBLIC_KEY"));
    }

    byte[] parsePrivateKey(byte[] bytes) {
        final String data = new String(bytes).replaceAll("\\r?\\n", "");

        final String begin = "-----BEGIN PRIVATE KEY-----";
        final String end = "-----END PRIVATE KEY-----";

        String key;

        if (data.startsWith(begin) && data.endsWith(end)) {
            key = data.substring(begin.length(), data.length() - end.length());
        } else {
            key = data;
        }

        return Base64.getDecoder().decode(key);
    }

    void loadPrivateKey(String filepath) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        final byte[] bytes = parsePrivateKey(Files.readAllBytes(Paths.get(filepath)));

        final PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(bytes);
        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        privateKey = keyFactory.generatePrivate(privateKeySpec);
    }

    void loadPublicKey(String filepath) throws CertificateException, FileNotFoundException {
        final FileInputStream stream = new FileInputStream(filepath);

        final CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
        final Certificate certificate = certificateFactory.generateCertificate(stream);

        publicKey = certificate.getPublicKey();
    }

    // public String generate(CustomUserPrincipal principal) {
    //     final Set<String> authorities = principal.getAuthorities().stream()
    //             .map(GrantedAuthority::getAuthority)
    //             .collect(Collectors.toSet());
    //
    //     final Date expiration = new Date(System.currentTimeMillis() + 1000 * 3600);
    //
    //     return Jwts.builder()
    //             .setSubject(principal.getUserID().toString())
    //             .claim(USERNAME, principal.getUsername())
    //             .claim(AUTHORITIES, authorities)
    //             .setExpiration(expiration)
    //             .signWith(privateKey)
    //             .compact();
    // }

    public Jws<Claims> parse(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(publicKey)
                .setAllowedClockSkewSeconds(60)
                .build()
                .parseClaimsJws(token);
    }
}
