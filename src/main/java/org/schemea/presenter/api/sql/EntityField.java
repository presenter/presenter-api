package org.schemea.presenter.api.sql;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.postgresql.jdbc.PgArray;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

@Builder
@RequiredArgsConstructor
public class EntityField {
    final private Object entity;
    final private Class<?> entityClass;

    final private String name;
    final private Field field;

    public Object getEntity() {
        return entity;
    }

    private Method getMethod() {
        try {
            return entityClass.getMethod("get" + name.substring(0, 1).toUpperCase() + name.substring(1));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public boolean canAccess() {
        if (Modifier.isPublic(field.getModifiers())) {
            return true;
        } else {
            return Modifier.isPublic(getMethod().getModifiers());
        }
    }

    public Object getValue() {
        try {
            if (field.canAccess(entity)) {
                return field.get(entity);
            } else {
                final Method method = getMethod();

                if (Objects.isNull(method)) {
                    throw new RuntimeException("cannot access field: " + field.getName());
                }

                return method.invoke(entity);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setValue(Object ent, Object value) {
        if (Objects.isNull(value)) {
            return;
        }


        try {
            if (value instanceof PgArray) {
                value = Arrays.asList((Object[]) ((PgArray) value).getArray());
            }

            if (field.canAccess(ent)) {
                field.set(ent, value);
            } else {
                final Method method = entityClass.getMethod("set" + name.substring(0, 1).toUpperCase() + name.substring(1), field.getType());
                method.invoke(ent, value);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | SQLException e) {
            e.printStackTrace();
        }
    }

    public <T extends Annotation> T getAnnotation(Class<T> aClass) {
        if (Modifier.isPublic(field.getModifiers())) {
            return field.getAnnotation(aClass);
        } else {
            final T annotation = getMethod().getAnnotation(aClass);
            return Objects.nonNull(annotation) ? annotation : field.getAnnotation(aClass);
        }
    }

    boolean hasPersistence(SqlOperation operation) {
        final Persistence persistence = getAnnotation(Persistence.class);

        if (Objects.isNull(persistence)) {
            return true;
        }

        return Arrays.asList(persistence.value()).contains(operation);
    }
}
