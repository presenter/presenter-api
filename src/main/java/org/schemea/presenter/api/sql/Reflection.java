package org.schemea.presenter.api.sql;

import java.util.*;
import java.util.stream.Collectors;

public class Reflection {

    public static List<EntityField> getFields(Class<?> entityClass, Object entity) {
        return Arrays.stream(entityClass.getDeclaredFields())
                .map(
                        field -> EntityField.builder()
                                .entity(entity)
                                .entityClass(entityClass)
                                .field(field)
                                .name(field.getName())
                                .build()
                )
                .filter(EntityField::canAccess)
                .collect(Collectors.toList());
    }
    public static List<EntityField> getFields(Class<?> entityClass) {
        return getFields(entityClass, null);
    }
}
