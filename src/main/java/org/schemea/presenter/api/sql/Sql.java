package org.schemea.presenter.api.sql;

import org.schemea.presenter.api.config.Database;

import java.lang.reflect.*;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sql {
    public static Connection getConnection() {
        return Database.getSessionManager().getConnection();
    }

    private static String getSqlType(Type type) {
        if (type.getTypeName().equals(UUID.class.getTypeName())) {
            return "uuid";
        }

        throw new RuntimeException("unknown type: " + type.getTypeName());
    }

    private static String placeholders(int count) {
        return Stream.generate(() -> "?")
                .limit(count)
                .collect(Collectors.joining(","));
    }

    private static String placeholders(Collection<?> collection) {
        return placeholders(collection.size());
    }

    private static String columns(Collection<EntityField> fields) {
        return fields.stream()
                .map(EntityField::getName)
                .collect(Collectors.joining(","));
    }

    public static <E> UUID insert(String table, E entity) {
        final List<EntityField> fields = Reflection.getFields(entity.getClass(), entity).stream()
                .filter(value -> value.hasPersistence(SqlOperation.INSERT))
                .filter(value -> Objects.nonNull(value.getValue()))
                .collect(Collectors.toList());

        try (PreparedStatement statement = getConnection().prepareStatement(MessageFormat.format(
                "INSERT INTO {0}({1}) VALUES({2}) RETURNING id",
                table,
                columns(fields),
                placeholders(fields)
        ))) {

            setStatementFields(statement, fields);

            try (final ResultSet rs = statement.executeQuery()) {
                if (!rs.next()) {
                    throw new RuntimeException("no value returned from the insert statement");
                }

                return rs.getObject(1, UUID.class);
            }
        } catch (SQLException e) {
            throw new SqlException(e);
        }
    }

    public static <E> List<E> select(String table, Class<E> entityClass, E filter) {
        try {
            final List<EntityField> fields = Reflection.getFields(entityClass).stream()
                    .filter(value -> value.hasPersistence(SqlOperation.SELECT))
                    .collect(Collectors.toList());

            final List<EntityField> where = Objects.nonNull(filter) ?
                    Reflection.getFields(entityClass, filter).stream()
                            .filter(value -> value.hasPersistence(SqlOperation.WHERE))
                            .filter(value -> Objects.nonNull(value.getValue()))
                            .collect(Collectors.toList())
                    : List.of();

            List<E> results;

            try (PreparedStatement statement = getConnection().prepareStatement(MessageFormat.format(
                    "SELECT {0} FROM {1} {2}",
                    columns(fields),
                    table,
                    where.isEmpty() ? "" : "WHERE " + where.stream()
                            .map(value -> MessageFormat.format("{0} = ?", value.getName()))
                            .collect(Collectors.joining(" AND "))
            ))) {

                setStatementFields(statement, where);

                try (ResultSet rs = statement.executeQuery()) {

                    results = new ArrayList<>();

                    while (rs.next()) {
                        final E entity = entityClass.getDeclaredConstructor().newInstance();

                        for (int i = 0; i < fields.size(); i++) {
                            final EntityField field = fields.get(i);
                            field.setValue(entity, rs.getObject(i + 1));
                        }

                        results.add(entity);
                    }
                }
            }

            return results;
        } catch (SQLException e) {
            throw new SqlException(e);
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setStatementFields(PreparedStatement statement, List<EntityField> fields, int offset) throws SQLException {
        Connection connection = statement.getConnection();

        for (int i = 0; i < fields.size(); i++) {
            final EntityField value = fields.get(i);

            if (value instanceof Collection) {
                final Object[] array = ((Collection<?>) value).toArray();
                final String type = getSqlType(array.getClass().getGenericInterfaces()[0]);

                statement.setArray(i + offset + 1, connection.createArrayOf(type, array));
            } else {
                statement.setObject(i + offset + 1, value.getValue());
            }
        }
    }

    public static void setStatementFields(PreparedStatement statement, List<EntityField> fields) throws SQLException {
        setStatementFields(statement, fields, 0);
    }
}
