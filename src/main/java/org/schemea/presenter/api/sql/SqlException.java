package org.schemea.presenter.api.sql;

class SqlException extends RuntimeException {
    public SqlException(Exception cause) {
        super(cause);
    }
}
