package org.schemea.presenter.api.sql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Getter
@Builder
@AllArgsConstructor
public class SqlFields {
    final private ArrayList<String> columns = new ArrayList<>();
    final private ArrayList<Object> values = new ArrayList<>();

    public String getPlaceholderString() {
        return getValues().stream()
                .map(o -> "?")
                .collect(Collectors.joining(","));
    }
}
