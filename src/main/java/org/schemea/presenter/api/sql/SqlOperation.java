package org.schemea.presenter.api.sql;

public enum SqlOperation {
    SELECT,
    INSERT,
    UPDATE,
    DELETE,
    WHERE
}
