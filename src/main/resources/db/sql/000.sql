--liquibase formatted sql
--changeset dev:39ab246e-29ad-4542-a2cb-4306ab9baf77

CREATE SCHEMA scrumboard;

SET SEARCH_PATH = scrumboard, public;

ALTER USER CURRENT_USER SET SEARCH_PATH = scrumboard, public;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
