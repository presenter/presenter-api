--liquibase formatted sql
--changeset dev:4028b881-74a7-5f01-0174-a75fabf60002

SET SEARCH_PATH = scrumboard, public;

CREATE TABLE projects
(
    id               uuid DEFAULT uuid_generate_v4(),
    name             varchar(100) NOT NULL UNIQUE,
    default_snapshot uuid         NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE snapshots
(
    id          uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    project_id  uuid        NOT NULL,
    timestamp   timestamptz      DEFAULT CURRENT_TIMESTAMP,
    name        varchar(20) NOT NULL,
    description varchar(100),
    FOREIGN KEY (project_id) REFERENCES projects (id),
    UNIQUE (project_id, name)
);

ALTER TABLE projects
    ADD FOREIGN KEY (default_snapshot) REFERENCES snapshots (id) INITIALLY DEFERRED;

CREATE TABLE stories
(
    id          uuid DEFAULT uuid_generate_v4(),
    snapshot_id uuid         NOT NULL,
    title       varchar(100) NOT NULL,
    description varchar(500),
    status_id   uuid,
    effort      int2,
    value       int2,
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (snapshot_id) REFERENCES snapshots (id)
);

CREATE TABLE members
(
    id        uuid DEFAULT uuid_generate_v4(),
    firstname varchar(20) NOT NULL,
    lastname  varchar(20) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (firstname, lastname)
);

CREATE TABLE team_roles
(
    id          uuid DEFAULT uuid_generate_v4(),
    snapshot_id uuid        NOT NULL,
    name        varchar(20) NOT NULL UNIQUE,
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (snapshot_id) REFERENCES snapshots (id)
);

CREATE TABLE boards
(
    id          uuid DEFAULT uuid_generate_v4(),
    snapshot_id uuid         NOT NULL,
    name        varchar(100) NOT NULL,
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (snapshot_id) REFERENCES snapshots (id)
);

CREATE TABLE teams
(
    id          uuid DEFAULT uuid_generate_v4(),
    snapshot_id uuid        NOT NULL,
    name        varchar(20) NOT NULL UNIQUE,
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (snapshot_id) REFERENCES snapshots (id)
);

CREATE TABLE statuses
(
    id          uuid   DEFAULT uuid_generate_v4(),
    snapshot_id uuid        NOT NULL,
    board_id    uuid        NOT NULL,
    name        varchar(20) NOT NULL,
    stories     uuid[] DEFAULT '{}',
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (snapshot_id) REFERENCES snapshots (id),
    FOREIGN KEY (board_id, snapshot_id) REFERENCES boards (id, snapshot_id),
    UNIQUE (board_id, name, snapshot_id)
);

ALTER TABLE stories
    ADD FOREIGN KEY (status_id, snapshot_id) REFERENCES statuses (id, snapshot_id);

CREATE TABLE users
(
    id       uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    username varchar(100) NOT NULL UNIQUE
);

CREATE TABLE access_roles
(
    id   uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(30) NOT NULL UNIQUE
);

-- JOIN TABLES

CREATE TABLE story_assignee
(
    story_id    uuid NOT NULL,
    assignee_id uuid NOT NULL,
    snapshot_id uuid NOT NULL,
    PRIMARY KEY (snapshot_id, story_id, assignee_id),
    FOREIGN KEY (story_id, snapshot_id) REFERENCES stories (id, snapshot_id),
    FOREIGN KEY (assignee_id) REFERENCES members (id)
);

CREATE TABLE project_team
(
    team_id     uuid NOT NULL,
    snapshot_id uuid NOT NULL,
    PRIMARY KEY (snapshot_id, team_id),
    FOREIGN KEY (team_id, snapshot_id) REFERENCES teams (id, snapshot_id)
);

CREATE TABLE team_member
(
    id          uuid DEFAULT uuid_generate_v4(),
    snapshot_id uuid NOT NULL,
    team_id     uuid NOT NULL,
    member_id   uuid NOT NULL,
    PRIMARY KEY (id, snapshot_id),
    FOREIGN KEY (team_id, snapshot_id) REFERENCES teams (id, snapshot_id),
    FOREIGN KEY (member_id) REFERENCES members (id),
    UNIQUE (team_id, member_id, snapshot_id)
);

CREATE TABLE team_member_team_role
(
    team_member_id uuid NOT NULL,
    role_id        uuid NOT NULL,
    snapshot_id    uuid,
    PRIMARY KEY (team_member_id, role_id, snapshot_id),
    FOREIGN KEY (team_member_id, snapshot_id) REFERENCES team_member (id, snapshot_id),
    FOREIGN KEY (role_id, snapshot_id) REFERENCES team_roles (id, snapshot_id)
);

CREATE TABLE user_access_role
(
    user_id    uuid NOT NULL REFERENCES users (id),
    role_id    uuid NOT NULL REFERENCES access_roles (id),
    project_id uuid NOT NULL REFERENCES projects (id),
    PRIMARY KEY (user_id, project_id, role_id)
);

CREATE TYPE auth_type AS ENUM ('password', 'token', 'none');

CREATE TABLE project_access_role
(
    secret     varchar(100) PRIMARY KEY,
    auth       auth_type,
    role_id    uuid NOT NULL REFERENCES access_roles (id),
    project_id uuid NOT NULL REFERENCES projects (id)
);

-- ACCESSES

REVOKE UPDATE ON TABLE team_member FROM CURRENT_USER;

-- ROLES

INSERT INTO access_roles(name)
VALUES ('owner'),
       ('editor'),
       ('viewer');
