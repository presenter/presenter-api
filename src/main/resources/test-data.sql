DO
$$
    DECLARE
        project uuid DEFAULT 'e309cffb-d233-4039-9823-d9a199f31b16';
        v0      uuid DEFAULT 'e28840cf-d970-46ce-a3d1-290efb8c13b6';
        v1      uuid DEFAULT 'cb35e819-c9e3-4075-bfbd-3c72f351c253';
        board   uuid DEFAULT '2a4895cf-f7b6-404d-b3e4-7a7a4928b2b8';
        status  uuid DEFAULT '75bff66c-9aa1-492c-b285-93ae0b0a8d34';
        story   uuid DEFAULT '860a5a48-65b1-45c4-b49b-304bca4280a6';
    BEGIN
        INSERT INTO projects(id, name, default_snapshot) VALUES (project, 'DELTA', v1);
        INSERT INTO snapshots(id, project_id, name, timestamp) VALUES (v0, project, 'v0', '1995-01-01');
        INSERT INTO snapshots(id, project_id, name, timestamp) VALUES (v1, project, 'v1', '1995-01-02');
        INSERT INTO boards(id, snapshot_id, name) VALUES (board, v1, 'BOAR');
        INSERT INTO statuses(id, snapshot_id, board_id, name) VALUES (status, v1, board, 'PENDING');
        INSERT INTO stories(id, snapshot_id, status_id, title) VALUES (story, v1, status, 'A Simple Story');
    END;
$$ LANGUAGE plpgsql;
